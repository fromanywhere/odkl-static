/*jslint browser: true */
/*global require */
requirejs.config({
    baseUrl: 'res/js/app',
    paths: {
        'OK/utils': 'utils'
    }
});

(function () {
    'use strict';

    function activate(element) {
        var
            /** @type {string} */
            moduleName = element.getAttribute('data-module'),
            /** @type {{}} */
            instance;

        require([moduleName], function (Module) {
            if (typeof Module === 'function') {
                instance = new Module(element.id);
            } else {
                instance = Module;
            }
            if (typeof instance.activate === 'function') {
                instance.activate(element);
            }
        });
    }


    var
        /** @type {HTMLElement[]} */
        hooks = Array.prototype.slice.call(document.getElementsByClassName('h-mod')),
        /** @type {number} */
        ind;
    for (ind = 0; ind < hooks.length; ind += 1) {
        activate(hooks[ind]);
    }
}());