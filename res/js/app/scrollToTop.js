/*jslint browser:true */
/*global define, OK, window */
/**
 * @module OK/scrollToTop
 */
define(['OK/utils/dom', 'OK/utils/throttle'], function (dom, throttle) {
    'use strict';

    var
        /** @type {string} */
        CSS_BOTTOM = '__bottom',
        /** @type {string} */
        CSS_HIDDEN = '__hidden',
        /** @type {string} */
        CSS_ACTIVE = '__active',
        /** @type {string} */
        CSS_ANIMATED = '__animated',
        /** @type {string} */
        CSS_NEW = '__new',
        /** @type {string} */
        DATA_HEIGHT = 'data-height',
        /** @type {string} */
        DATA_CONTAINER = 'data-container',
        /** @type {string} */
        DATA_NEW_ON_SCROLL_UP = 'data-check-up',
        /** @type {string} */
        DATA_PAUSE_TO_NEW = 'data-max-pause';

    /**
     * @constructor
     * @alias module:OK/scrollToTop
     */
    function ScrollToTop() {
        /** @type {HTMLElement} */
        this.button = null;
        /** @type {HTMLElement} */
        this.container = null;
    }

    /**
     * @param {HTMLElement} element
     */
    ScrollToTop.prototype.activate = function activate(element) {
        /** @type {HTMLElement} */
        this.button = element;
        /** @type {boolean} */
        this.showBottom = this.button.hasAttribute(DATA_CONTAINER);
        /** @type {HTMLElement} */
        this.container = this.showBottom
            ? document.getElementById(this.button.getAttribute(DATA_CONTAINER))
            : window;

        // По какой-то причине контейнера нет в дереве
        if (!this.container) {
            return;
        }

        /** @type {number} */
        this.scrollToTopHeight = parseInt(this.button.getAttribute(DATA_HEIGHT) || 0, 10);
        /** @type {number} */
        this.scrollToBottomHeight = this.scrollToTopHeight;
        /** @type {boolean} */
        this.isBottom = this.button.classList.contains(CSS_BOTTOM);
        /** @type {HTMLElement} */
        this.link = dom.firstByClass(this.button, 'scroll-arrow_lk');
        /** @type {number} */
        this.newOnScrollUp = parseInt(this.button.getAttribute(DATA_NEW_ON_SCROLL_UP) || 0, 10);
        /** @type {number} */
        this.pauseToNew =  parseInt(element.getAttribute(DATA_PAUSE_TO_NEW) || 0, 10);
        /** @type {number} */
        this.onScrollSeenTimer = null;
        /** @type {number} */
        this.scrollStartTime = 0;
        /** @type {number} */
        this.lastScrollTopPosition = 0;

        this.boundClickHandler = this.onClick.bind(this);
        this.boundMouseEnterHandler = this.onMouseEnter.bind(this);
        this.boundMouseLeaveHandler = this.onMouseLeave.bind(this);
        this.boundScrollHandler = throttle(10, this.onScroll.bind(this));
        this.boundHide = this.hide.bind(this);
        this.boundHighlightNew = this.highlightNew.bind(this);

        this.button.addEventListener('click', this.boundClickHandler);
        this.button.addEventListener('mouseenter', this.boundMouseEnterHandler);
        this.button.addEventListener('mouseleave', this.boundMouseLeaveHandler);
        this.button.addEventListener('hideScrollElement', this.boundHide);

        this.container.addEventListener('scroll', this.boundScrollHandler);
    };

    ScrollToTop.prototype.deactivate = function deactivate() {
        this.button.classList.remove(CSS_ACTIVE, CSS_ANIMATED);
        if (!this.container) {
            return;
        }
        clearTimeout(this.onScrollSeenTimer);

        this.button.addEventListener('click', this.boundClickHandler);
        this.button.addEventListener('mouseenter', this.boundMouseEnterHandler);
        this.button.addEventListener('mouseleave', this.boundMouseLeaveHandler);
        this.button.addEventListener('hideScrollElement', this.boundHide);

        this.button = null;

        this.container.removeEventListener('scroll', this.boundScrollHandler);
    };

    ScrollToTop.prototype.getScrollTop = function () {
        return this.container === window
            ? window.pageYOffset
            : this.container.scrollTop;
    };

    ScrollToTop.prototype.setScrollTop = function (value) {
        if (this.container === window) {
            window.scrollTo(0, value);
        } else {
            this.container.scrollTop = value;
        }
    };

    ScrollToTop.prototype.onMouseEnter = function () {
        if (this.isBottom && this.getScrollTop() >= this.scrollToTopHeight) {
            this.button.classList.remove(CSS_HIDDEN);
        }
    };

    ScrollToTop.prototype.onMouseLeave = function () {
        if (this.isBottom) {
            this.button.classList.add(CSS_HIDDEN);
        }
    };

    ScrollToTop.prototype.highlightNew = function () {
        this.button.classList.add(CSS_NEW);
    };

    ScrollToTop.prototype.onScroll = function() {
        var scrollTopPosition = this.getScrollTop();
        var now = Date.now();

        if (this.scrollStartTime && this.newOnScrollUp && this.scrollStartTime + this.newOnScrollUp < now) {
            if (this.newOnScrollUp && this.lastScrollTopPosition > scrollTopPosition) {
                this.highlightNew();
            }
        } else if (!this.scrollStartTime && this.newOnScrollUp) {
            this.scrollStartTime = now;
        }

        this.lastScrollTopPosition = scrollTopPosition;

        if (this.onScrollSeenTimer) {
            clearTimeout(this.onScrollSeenTimer);
        }

        if (this.pauseToNew) {
            this.onScrollSeenTimer = setTimeout(this.boundHighlightNew, this.pauseToNew);
        }

        if (scrollTopPosition > 550) {
            this.button.classList.remove(CSS_BOTTOM, CSS_HIDDEN);
            this.button.classList.add(CSS_ACTIVE, CSS_ANIMATED);
            this.isBottom = false;
        } else if (scrollTopPosition < 500 && !this.isBottom) {
            this.hide();
        }
    };

    ScrollToTop.prototype.hide = function () {
        this.button.classList.remove(CSS_ACTIVE);
    };

    ScrollToTop.prototype.onClick = function (e) {
        if (this.isBottom) {
            this.setScrollTop(this.scrollToBottomHeight);
            this.button.classList.remove(CSS_BOTTOM);
            this.isBottom = false;
        } else {
            this.scrollToBottomHeight = this.getScrollTop();
            this.setScrollTop(this.scrollToTopHeight);
            if (this.showBottom) {
                this.button.classList.add(CSS_BOTTOM);
                this.isBottom = true;
            }
        }
        this.onScroll();

        if (this.button.classList.contains(CSS_NEW) && this.link) {
            this.link.click();
        }

        return OK.stop(e);
    };

    return ScrollToTop;
});