/*jslint browser: true */
/*jslint white: true */
/*global define */
/**
 * @module OK/utils/dom
 */
define(function () {
    'use strict';
    /**
     * @param {HTMLElement} root
     * @param {string} className
     * @returns {HTMLElement[]}
     */
    function byClass(root, className) {
        if (root) {
            if (root.nodeType == 11) { // == Node.DOCUMENT_FRAGMENT_NODE
                var result = [];
                for (var df = 0; df < root.childNodes.length; df++) {
                    var childNode = root.childNodes[df];
                    if (childNode.getElementsByClassName) {
                        result = result.concat(Array.prototype.slice.call(childNode.getElementsByClassName(className)));
                    }
                }
                return result;
            }
            return Array.prototype.slice.call(root.getElementsByClassName(className));
        }
        return [];
    }

    /**
     * @param {HTMLElement} root
     * @param {string} className
     * @returns {?HTMLElement}
     */
    function firstByClass(root, className) {
        var elements = byClass(root, className);
        return elements.length ? elements[0] : null;
    }

    /**
     * @param {HTMLElement} element
     * @param {string} [className]
     * @param {?HTMLElement} [limitElement]
     * @param {?boolean} [startFromEl] начинать поиск с самого элемента, а не его родителя
     * @returns {HTMLElement}
     */
    function parent(element, className, limitElement, startFromEl) {
        if (!startFromEl) {
            element = element.parentElement;
        }
        while (element) {
            if (!className || !element.classList || element.classList.contains(className)) {
                return element;
            }
            if (limitElement && (limitElement === element)) {
                return null;
            }
            element = element.parentElement;
        }
        return element;
    }

    /**
     * Traverses elements from a given element to doc root until condition predicate is not satisfied
     * @param {HTMLElement} element starting element
     * @param {Function} stopCondition stop condition
     * @param {?HTMLElement} limitElement limiting element or null
     * @param {?boolean} [startFromEl] начинать поиск с самого элемента, а не его родителя
     * @returns {HTMLElement} last element in chain or null
     */
    function traverseParents(element, stopCondition, limitElement, startFromEl) {
        if (!startFromEl) {
            element = element.parentElement;
        }
        while (element) {
            var res = stopCondition(element);
            if (res) {
                return element;
            }
            if (limitElement && (limitElement === element)) {
                return null;
            }
            element = element.parentElement;
        }
        return element;
    }

    /**
     * Traverse parents until find attribute and retur it's value
     *
     * @param {HTMLElement} element starting element
     * @param {string} attributeName stop condition
     * @param {?HTMLElement} limitElement limiting element or null
     * @returns {string|null} attribute value
     */
    function getParentsAttrValue(element, attributeName, limitElement) {
        var getAttributeValue = function(el) {
                return el.getAttribute(attributeName);
            },
            target = traverseParents(element, getAttributeValue, limitElement, true);

        return target ? getAttributeValue(target) : null;
    }

    /**
     * Removes elements from DOM
     * @param {HTMLElement|HTMLElement[]} elements — single element or array to remove from DOM
     */
    function remove(elements) {
        if (!Array.isArray(elements)) {
            elements = [elements];
        }

        var parent;
        elements.forEach(function(el) {
            parent = el.parentNode;
            if (parent) {
                parent.removeChild(el);
            }
        });

    }

    /**
     * Removes all listeners from elements by cloning it
     * @param {HTMLElement} el
     *
     * @returns {Node} new element
     */
    function removeAllListeners(el) {
        var elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);

        return elClone;
    }

    return /** @alias module:OK/utils/dom */ {
        byClass: byClass,
        firstByClass: firstByClass,
        parent: parent,
        traverseParents: traverseParents,
        getParentsAttrValue: getParentsAttrValue,

        /**
         * @param {HTMLCollection} collection
         * @param {Function} fun
         * @return {HTMLCollection}
         */
        forEach: function (collection, fun) {
            Array.prototype.slice.call(collection).forEach(fun);
            return collection;
        },

        innerHeight: function (el) {
            try {
                return window.getComputedStyle(el, null).getPropertyValue('height');
            } catch (e) {
                return el.currentStyle.height;
            }
        },

        /**
         * Условный аналог $.outerWidth/outerHeught
         * Не рекомендуется использовать на скрытом (display: none) элементе
         * @param {HTMLElement} el сам элемент
         * @param {boolean} [isWidth] считать ширину или высоту
         * @param {boolean} [includeMargins] включать ли margin в расчет
         * @returns {*}
         */
        outerSize: function (el, isWidth, includeMargins) {
            var result = isWidth
                ? el.offsetWidth
                : el.offsetHeight;

            if (result !== null) {
                if (includeMargins) {
                    var style = window.getComputedStyle
                        ? window.getComputedStyle(el, null)
                        : el.currentStyle;

                    result += isWidth
                        ? parseInt(style.marginLeft, 10) + parseInt(style.marginRight, 10)
                        : parseInt(style.marginTop, 10) + parseInt(style.marginBottom, 10);
                }
                return result;
            }
            return 0;
        },

        remove: remove,

        one: function (node, type, callback) {
            node.addEventListener(type, function handler(e) {
                e.target.removeEventListener(e.type, handler);
                return callback.call(this, e);
            });
        },

        removeAllListeners: removeAllListeners
    };
});