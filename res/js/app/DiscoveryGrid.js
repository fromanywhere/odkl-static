/*jslint browser:true */
/*global define, OK, Promise */
/**
 * @module OK/DiscoveryGrid
 */
define(['OK/utils/dom', 'OK/utils/throttle'], function (dom, throttle) {
    'use strict';

    var windowHeight = window.innerHeight;
    var setWindowHeight = throttle(100, function () {
        windowHeight = window.innerHeight;
    });

    /**
     * @class DiscoveryGrid
     * @constructor
     */
    function DiscoveryGrid() {}

    /**
     *
     * @param {number} itemTop
     * @param {number} itemBottom
     * @param {number} frameTop
     * @param {number} frameBottom
     * @returns {boolean}
     */
    function intersectRect(itemTop, itemBottom, frameTop, frameBottom) {
        // Оптимизируем отсечения, используя ленивые логические операции
        // Скорее всего снизу элементов будет меньше, чем сверху,
        // поэтому сначала нужно проверять пересечение по верхней границе
        // (все элементы сверху в любом случае будут выше нижней границы)
        return (itemBottom > frameTop) && (itemTop < frameBottom);
    }

    DiscoveryGrid.prototype.initColumnsHeight = function () {
        // Сбросим высоты колонок
        for (var columnCount = 0; columnCount < this.columns.length; columnCount++) {
            this.columnsHeight[columnCount] = 0;
        }

        // Частный случай первого внешнего блока, обладающего влиянием на остальные
        var myCardElement = dom.firstByClass(this.loaderContainer, 'photo-wall-my-card');
        if (myCardElement) {
            this.columnsHeight[0] = dom.outerSize(myCardElement, false, true);
        }
    };

    DiscoveryGrid.prototype.frameChecking = function () {
        var status;
        // Вычесть расположение лоадера. Предполагается, что его позиция не меняется
        var scrollPosition = window.pageYOffset - this.gridOffset;
        var frameTop = scrollPosition - windowHeight * this.frameOffset;
        // Нижняя граница — скролл + сам вьюпорт + коэффициент
        var frameBottom = scrollPosition + windowHeight * (this.frameOffset + 1);
        this.items.forEach(function (item) {
            status = intersectRect(item.top, item.bottom, frameTop, frameBottom);
            if (status !== item.active) {
                item.element.classList.toggle('invisible', !status);
                item.active = status;
            }
        });
    };

    /**
     * Рассчитывает высоты колонок и элементок
     * @param {Array.<HTMLElement>} elements
     * @returns {Promise.<Array.<number>>}
     */
    DiscoveryGrid.prototype.getItemsHeight = function (elements) {
        return new Promise(function (resolve) {
            // Разделим вставку элементов и пересчет их стилей
            window.requestAnimationFrame(function () {
                var itemsHeights = [];
                var i;

                for (i = 0; i < elements.length; i++) {
                    itemsHeights.push(dom.outerSize(elements[i], false, true));
                }

                resolve(itemsHeights);
            });
        });
    };

    /**
     * @method Распихиваем элементы по колонкам
     * @param {Array.<HTMLElement>} elements
     */
    DiscoveryGrid.prototype.putElementsToGrid = function (elements) {
        var columns = this.columns;
        var columnsHeight = this.columnsHeight;
        var items = this.items;
        var staticLayout = this.staticLayout;
        var i, smallestColumnHeight, smallestColumnIndex, item;

        if (this.redesign) {
            this.getItemsHeight(elements).then(function (itemsHeights) {

                for (i = 0; i < elements.length; i++) {
                    item = elements[i];
                    // Найдем самую маленькую колонку
                    smallestColumnHeight = Math.min.apply(null, columnsHeight);
                    smallestColumnIndex = columnsHeight.indexOf(smallestColumnHeight);

                    // Увеличим её на высоту элемента
                    columnsHeight[smallestColumnIndex] += itemsHeights[i];

                    // Перемещаем элемент
                    columns[smallestColumnIndex].insertBefore(item, null);
                    if (staticLayout) {
                        item.setAttribute('style', ['position: absolute', 'left: 0px', 'top: ' + smallestColumnHeight + 'px'].join(';'));

                        items.push({
                            element: item,
                            top: smallestColumnHeight,
                            bottom: smallestColumnHeight + itemsHeights[i],
                            height: itemsHeights[i],
                            active: undefined
                        });
                    }
                }

                // Изменим высоты колонок, незачем это делать в цикле
                if (staticLayout) {
                    columns.forEach(function (column, index) {
                        column.style.height = columnsHeight[index] + 'px';
                    })
                }
            });
        } else {
            var firstCol = this.columns[0],
                secondCol = this.columns[1],
                grid = dom.firstByClass(this.loaderContainer, 'ugrid_cnt');

            elements.forEach(function (item) {
                item = grid.removeChild(item);
                if (firstCol.clientHeight <= secondCol.clientHeight) {
                    firstCol.appendChild(item);
                } else {
                    secondCol.appendChild(item);
                }
            });
        }

        this.showElements(elements);
    };

    /**
     * @method Отображаем элементы
     */
    DiscoveryGrid.prototype.showElements = function (elements) {
        var newClass = this.newModificatorClass;
        if (elements.length) {
            if (this.staticLayout) {
                this.frameChecking();
            }
            window.setTimeout(function () {
                elements.forEach(function (item) {
                    item.classList.remove(newClass);
                });
            }, 1);
        }
    };

    /**
     * @method Активация модуля
     * @param {HTMLElement} element
     */
    DiscoveryGrid.prototype.activate  = function (element) {
        this.loaderClass = element.getAttribute('data-loader-class') || 'loader-container';
        this.columnClass = element.getAttribute('data-column-class');
        this.newItemsClass = element.getAttribute('data-new-items-class');
        this.newModificatorClass = element.getAttribute('data-new-mod-class');
        this.redesign = !!element.getAttribute('data-redesign');
        this.staticLayout = !!element.getAttribute('data-static-layout');
        this.frameOffset = Number(element.getAttribute('data-frame-offset')) || 2;
        this.staticLayoutScroll = throttle(100, this.frameChecking.bind(this));
        this.gridOffset = element.getBoundingClientRect().top + window.pageYOffset;

        this.loaderContainer = dom.firstByClass(element, this.loaderClass) || element;
        this.columns = dom.byClass(this.loaderContainer, this.columnClass);
        this.columnsHeight = [];
        this.items = [];

        this.initColumnsHeight();

        this.onDataLoad = function (e) {
            if (e) {
                var newElements = e.detail.elements;
                if (!newElements) {
                    return;
                }
                this.putElementsToGrid(newElements);
            } else {
                var firstChunk = dom.byClass(this.loaderContainer, this.newItemsClass);
                this.putElementsToGrid(firstChunk);
            }
        }.bind(this);

        if (this.loaderContainer) {
            this.loaderContainer.addEventListener('dataload', this.onDataLoad);
        }

        if (this.staticLayout) {
            setWindowHeight();
            window.addEventListener('scroll', this.staticLayoutScroll);
            window.addEventListener('resize', setWindowHeight);
        }

        this.onDataLoad();
    };

    /**
     * @method Деактивация модуля
     */
    DiscoveryGrid.prototype.deactivate = function () {
        if (this.loaderContainer) {
            this.loaderContainer.removeEventListener('dataload', this.onDataLoad);
        }

        if (this.staticLayout) {
            window.removeEventListener('scroll', this.staticLayoutScroll);
            window.removeEventListener('resize', setWindowHeight);
        }
    };

    return DiscoveryGrid;
});