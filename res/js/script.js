window.OK = {
    scrollBar: function () {
        OK.scrollBar.width = document.getElementById('sc').offsetWidth - document.getElementById('sch').offsetWidth;
    },
    setContentWidth: function () {
        var scrollWidth = OK.scrollBar.width;
        var windowWidth = window.innerWidth - scrollWidth;

        // Setting min-width equal 1000 cause result window width must be (1000 + scrollWidth) < 1024
        windowWidth = Math.max(1000, windowWidth);
        var topPanel = document.getElementById('topPanel');
        var bodySwitcher = document.getElementById('hook_Block_BodySwitcher');

        // there are pages where can be not topPanel and bodySwitcher
        if (bodySwitcher) {
            bodySwitcher.style.width = windowWidth + 'px';
        }
        if (topPanel) {
            topPanel.style.width = windowWidth + 'px';
        }
    }
};